## Git

### Apprendre par la lecture

 * Tout sur git - le [livre progit fr v2](http://git-scm.com/book/fr/v2)
 * En 5 minutes - [*git petit guide*](http://rogerdudler.github.io/git-guide/index.fr.html)
 * [Schéma qui montre comment fonctionne le add/commit/push/pull/fetch](http://assets.osteele.com/images/2008/git-transport.png)
 * [Un schéma interactif](https://ndpsoftware.com/git-cheatsheet.html) qui montre les différentes "zones" (area) de git
 * [Git is hard: screwing up is easy, and figuring out how to fix your mistakes is fucking impossible.](https://ohshitgit.com)
 
### Tutoriels
 * [Github Hello World guide](https://guides.github.com/activities/hello-world/)

### Apprendre par la pratique
 * [tester git de manière graphique](https://lmazardo.github.io/explain-git-with-d3)
 * [Learn Git Branching](https://learngitbranching.js.org/) - Tutoriel visuel

### Divers
 * [Génération d'un gitignore approprié au type de projet](https://www.gitignore.io)
 * [Si vraiment vous ne comprennez pas git](https://imgs.xkcd.com/comics/git.png)
