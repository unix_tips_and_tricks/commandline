1. Associate file extension to excel attribute

```
~/.config/git/attributes
*.xlsx diff=excel
```

2. Add the following to your $HOME/.gitconfig
```
[diff "excel"]
  command = $HOME/bin/excel_cmp --diff_format=unified # you could find excel_cmp https://github.com/na-ka-na/ExcelCompare/releases
```

3. git diff A.xlsx
